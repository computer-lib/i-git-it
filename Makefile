DATE := $(shell date -I)

PANDOC_OPTIONS = --template templates/default.html \
	--standalone \
	--self-contained \
  --number-sections \
  --toc \
  -M date=$(DATE) \
	-t html5

DOCS = _build/workshop.html

_build/%.html: %.org templates/default.html
	mkdir -p _build
	pandoc $(PANDOC_OPTIONS) -o $@ $<

docs: $(DOCS)

.PHONY: clean
clean:
	rm -r _build/

